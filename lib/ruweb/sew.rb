# frozen_string_literal: true

require 'ruweb/help'
require 'ruweb/read'
require 'ruweb/sewsource'
require 'ruweb/sewcode'
require 'ruweb/execute'
require 'ruweb/save'

module RuWEB
  # Preprocesses the URI
  module Sew
    extend self

    def init(*opts)
      if STDIN.tty?
        @opts    = opts.first == nil ? ['--help'] : opts
        @man_uri = 'https://pad.programando.li/ruweb:manual/download'
        @uri     = obtain_uri
        RuWEB::Help.init if help?
      else
        @opts = opts
        @uri  = STDIN.read
      end
      run
    end
    
    private
    
    def help?
      [
        @opts.include?('--help'),
        @opts.include?('-h'),
        @opts.empty?,
        @uri == @man_uri
      ].include?(true)
    end
    
    def run
      raw    = STDIN.tty? ? RuWEB::Read.init(@uri) : @uri
      source = RuWEB::SewSource.init(raw)
      code   = RuWEB::SewCode.init(source)
      if @opts.any? { |opt| /^--save/ =~ opt }
        RuWEB::Save.init('raw.md',    raw)    if @opts.include?('--save-raw')
        RuWEB::Save.init('source.md', source) if @opts.include?('--save-source')
        RuWEB::Save.init('code.txt',  code)   if @opts.include?('--save-code')
      else
        RuWEB::Execute.init(code) unless @opts.include?('--save-code')
      end
    end
    
    def obtain_uri
      @opts.last[0..1] == '--' ? @man_uri : @opts.last
    end
  end
end